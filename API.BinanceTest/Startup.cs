﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.BinanceTest.Concentrate;
using API.BinanceTest.Configs;
using API.BinanceTest.HostedServices;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace API.BinanceTest
{
    public class Startup
    {

        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = Program.Configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton(Configuration);
            services.AddOptions();
            services.AddEntityFrameworkNpgsql()
                .AddDbContext<BinanceContext>(cfg =>
                    {
                        cfg.UseNpgsql(Configuration.GetConnectionString("PSQL"));
                    })
                .BuildServiceProvider();
            services.AddHostedService<BinanceGetInfoHostedService>();
            services.Configure<BinanceApiConfig>(Configuration.GetSection("BinanceApiConfig"));
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync("Hello World!");
            });
        }
    }
}
