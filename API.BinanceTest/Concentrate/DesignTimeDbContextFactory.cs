﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace API.BinanceTest.Concentrate
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<BinanceContext>
    {
        public BinanceContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.Development.json")
                .Build();
            var builder = new DbContextOptionsBuilder<BinanceContext>();
            var connectionString = configuration.GetConnectionString("PSQL");
            builder.UseNpgsql(connectionString);
            return new BinanceContext(builder.Options);
        }
    }
}
