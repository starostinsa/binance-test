﻿using API.BinanceTest.Models;
using Microsoft.EntityFrameworkCore;

namespace API.BinanceTest.Concentrate
{
    public class BinanceContext: DbContext
    {
        public BinanceContext() { Database.Migrate(); }
        public BinanceContext(DbContextOptions<BinanceContext> options) : base(options) { }
        public DbSet<BinanceHistory> BinanceHistory { get; set; }
    }
}
