﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.BinanceTest.Configs
{
    public class BinanceApiConfig
    {
        public string ApiKey { get; set; }
        public string SecretKey { get; set; }
    }
}
