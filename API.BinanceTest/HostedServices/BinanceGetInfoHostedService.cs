﻿using System;
using System.Threading;
using System.Threading.Tasks;
using API.BinanceTest.Concentrate;
using API.BinanceTest.Configs;
using API.BinanceTest.Models;
using BinanceExchange.API.Client;
using BinanceExchange.API.Websockets;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace API.BinanceTest.HostedServices
{
    public class BinanceGetInfoHostedService : IHostedService, IDisposable
    {
        private string Key { get; }
        private string Secret { get; }
        private BinanceClient Client { get; set; }
        private InstanceBinanceWebSocketClient WebSocketClient { get; set; }
        private Guid SocketId { get; set; }
        private readonly IServiceScopeFactory _scopeFactory;
        private bool inProcess;
        public BinanceGetInfoHostedService(IOptions<BinanceApiConfig> config, IServiceScopeFactory scopeFactory)
        {
            Key = config.Value.ApiKey;
            Secret = config.Value.SecretKey;
            Client = new BinanceClient(new ClientConfiguration()
            {
                ApiKey = Key,
                SecretKey = Secret,
            });
            WebSocketClient = new InstanceBinanceWebSocketClient(Client);
            _scopeFactory = scopeFactory;
        }
        public Task StartAsync(CancellationToken cancellationToken)
        {
            SocketId = WebSocketClient.ConnectToDepthWebSocket("ETHBTC", data =>
            {
                if(!inProcess) InsertData(data);
                Console.WriteLine($"DepthCall: {JsonConvert.SerializeObject(data)}");
            });
            return Task.CompletedTask;
        }
        public Task StopAsync(CancellationToken cancellationToken)
        {
            WebSocketClient.CloseWebSocketInstance(SocketId);
            return Task.CompletedTask;
        }
        public void Dispose()
        {
            if (WebSocketClient.IsAlive(SocketId))
                WebSocketClient.CloseWebSocketInstance(SocketId);
        }
        private void InsertData(object data)
        {
            using (var scope = _scopeFactory.CreateScope())
            {
                inProcess = true;
                var dbContext = scope.ServiceProvider.GetRequiredService<BinanceContext>();
                dbContext.BinanceHistory.Add(new BinanceHistory() { JsonDataAnswer = JsonConvert.SerializeObject(data) });
                dbContext.SaveChanges();
                inProcess = false;
            }
        }
    }
}
