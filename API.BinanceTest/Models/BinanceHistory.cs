﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.BinanceTest.Models
{
    public class BinanceHistory
    {
        public int Id { get; set; }
        public string JsonDataAnswer { get; set; }
    }
}
